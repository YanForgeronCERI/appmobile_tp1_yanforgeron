package com.example.tp1_yanforgeron;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import static com.example.tp1_yanforgeron.data.Country.countries;

public class DetailFragment extends Fragment {

    ImageView countryImage;
    TextView countryName;
    TextView langue;
    TextView capitale;
    TextView monnaie;
    TextView population;
    TextView superficie;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        int countryId = args.getCountryId();

        countryImage = view.findViewById(R.id.country_image);
        String imageName = countries[countryId].getImgUri();
        Context c = this.getContext();
        countryImage.setImageResource(c.getResources().getIdentifier(imageName , null , c.getPackageName()));

        countryName = view.findViewById(R.id.country_name);
        countryName.setText(countries[countryId].getName());

        capitale = view.findViewById(R.id.capitale);
        capitale.setText(countries[countryId].getCapital());

        langue = view.findViewById(R.id.langue);
        langue.setText(countries[countryId].getLanguage());

        monnaie = view.findViewById(R.id.monnaie);
        monnaie.setText(countries[countryId].getCurrency());

        population = view.findViewById(R.id.population);
        population.setText(Integer.toString(countries[countryId].getPopulation()));

        superficie = view.findViewById(R.id.superficie);
        superficie.setText(Integer.toString(countries[countryId].getArea()) + " km2");


        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}